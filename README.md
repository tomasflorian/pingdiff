# pingdiff #

This bash tool can estimate a link bandwidth by using nothing more than ICMP ping.  It is useful for cybersecurity reconnaissance and general troubleshooting without having to rely on server/client methods

## Usage ##

pingdiff.sh IP MAXPAYLOAD

* Experiment with MAX payload size between 100 and 64000 the bigger you can specify the more accurate the estimate will be
* The longer you let the loop running the more accurate the results will be in my experience the accuracy is +/- 50%
* Surprisingly the accuracy is not affected by link saturation as much as I would expect  Ex. 10Gbps link shows 4Gbps whether saturated or not
